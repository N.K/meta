#Dilwsi kai arxikopoihsh toy pinaka M
x=12
y=7
M=[[0 for j in range(y)]for i in range (x)]
#proti grammi
#------------------------------------
M[0][0]="[A]"
M[0][1]="F"
M[0][2]="F"
M[0][3]="F"
M[0][4]="F"
M[0][5]="F"
M[0][6]="F"
#------------------------------------
#deuteri grammi
#------------------------------------
M[1][0]="BE"
M[1][1]="F"
M[1][2]="BE"
M[1][3]="BE"
M[1][4]="F"
M[1][5]="F"
M[1][6]="F"
#------------------------------------
#triti grammi
#------------------------------------
M[2][0]="S"
M[2][1]="F"
M[2][2]="x"
M[2][3]="y"
M[2][4]="F"
M[2][5]="F"
M[2][6]="F"
#------------------------------------
#tetarti grammi
#------------------------------------
M[3][0]="F"
M[3][1]="empty"
M[3][2]="F"
M[3][3]="F"
M[3][4]=":A"
M[3][5]="+A"
M[3][6]="F"
#------------------------------------
#pempti grammi
#------------------------------------
M[4][0]="F"
M[4][1]="F"
M[4][2]="F"
M[4][3]="F"
M[4][4]="F"
M[4][5]="F"
M[4][6]="SUCCESS"
#------------------------------------
#ekti grammi
#------------------------------------
M[5][0]="N"
M[5][1]="F"
M[5][2]="F"
M[5][3]="F"
M[5][4]="F"
M[5][5]="F"
M[5][6]="F"
#------------------------------------
#evdomi grammi
#------------------------------------
M[6][0]="F"
M[6][1]="N"
M[6][2]="F"
M[6][3]="F"
M[6][4]="F"
M[6][5]="F"
M[6][6]="F"
#------------------------------------
#ogdoi grammi
#------------------------------------
M[7][0]="F"
M[7][1]="F"
M[7][2]="N"
M[7][3]="F"
M[7][4]="F"
M[7][5]="F"
M[7][6]="F"
#------------------------------------
#enati grammi
#------------------------------------
M[8][0]="F"
M[8][1]="F"
M[8][2]="F"
M[8][3]="N"
M[8][4]="F"
M[8][5]="F"
M[8][6]="F"
#------------------------------------
#dekati grammi
#------------------------------------
M[9][0]="F"
M[9][1]="F"
M[9][2]="F"
M[9][3]="F"
M[9][4]="N"
M[9][5]="F"
M[9][6]="F"
#------------------------------------
#endekati grammi
#------------------------------------
M[10][0]="F"
M[10][1]="F"
M[10][2]="F"
M[10][3]="F"
M[10][4]="F"
M[10][5]="N"
M[10][6]="F"
#------------------------------------
#dodekati grammi
#------------------------------------
M[11][0]="F"
M[11][1]="F"
M[11][2]="F"
M[11][3]="F"
M[11][4]="F"
M[11][5]="F"
M[11][6]="SUCCESS"
#------------------------------------

stoiva=["$","S"]
eisodos=raw_input("Insert the sequence of charachters to be analyzed(please don't forget the $ sign in the end): ")
continue_loop=True
result=False
korifi_stoivas=stoiva[-1]
proto_gramma_eisodou=eisodos[0]
grammi=0
stili=0
def telos(katastasi):
    global continue_loop
    global result
    if katastasi==0:
        continue_loop=False
        result=False
    elif katastasi==1:
        continue_loop=False
        result=True
def aferesi_omoiwn():
    global stoiva
    global eisodos
    stoiva.pop()
    eisodos=eisodos[1:]
def sinartisi_empty():
    global stoiva
    stoiva.pop()
def adikatastasi_kanona(x,y):
    global stoiva
    global M
    stoiva.pop()
    paragogi=M[x][y]
    paragogi=paragogi[::-1]
    for i in range(0,len(paragogi)):
        stoiva.append(paragogi[i])
def anagnorisi_grammis(x):
    if x=="S":
        return 0
    elif x=="A":
        return 1
    elif x=="B":
        return 2
    elif x=="E":
        return 3
    elif x=="$":
        return 4
    elif x=="[":
        return 5
    elif x=="]":
        return 6
    elif x=="x":
        return 7
    elif x=="y":
        return 8
    elif x==":":
        return 9
    elif x=="+":
        return 10
    else:
        return -1
def anagnorisi_stilis(y):
    if y=="[":
        return 0
    elif y=="]":
        return 1
    elif y=="x":
        return 2
    elif y=="y":
        return 3
    elif y==":":
        return 4
    elif y=="+":
        return 5
    elif y=="$":
        return 6
    else:
        return -1
def leitourgikotita_pinaka(sint_x,sint_y):
    global M
    if M[sint_x][sint_y]=="F":
        telos(0)
    elif M[sint_x][sint_y]=="SUCCESS":
        telos(1)
    elif M[sint_x][sint_y]=="N":
        aferesi_omoiwn()
    elif M[sint_x][sint_y]=="empty":
        sinartisi_empty()
    else:
        adikatastasi_kanona(sint_x,sint_y)
def proper_functionality():
    global eisodos
    global stoiva
    global continue_loop
    global korifi_stoivas
    global proto_gramma_eisodou
    while(continue_loop):
        korifi_stoivas=stoiva[-1]
        proto_gramma_eisodou=eisodos[0]
        grammi=anagnorisi_grammis(korifi_stoivas)
        stili=anagnorisi_stilis(proto_gramma_eisodou)
        #print("korifi_stoivas: "+korifi_stoivas)
        #print("proto_gramma_eisodou: "+proto_gramma_eisodou)
        #print(grammi)
        #print(stili)
        if(grammi and stili)>=0:
            leitourgikotita_pinaka(grammi,stili)
            print("--------------------------------")
            print("stoiva")
            for i in range(0,len(stoiva)):
                print stoiva[i]
            print("eisodos")
            print(eisodos)
            print("--------------------------------")
        else:
            telos(0)

proper_functionality()
if result==True:
    print("The sequence was recognized")
elif result==False:
    print("The sequence was NOT recognized")
