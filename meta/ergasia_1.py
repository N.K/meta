#!/usr/bin/
import re
#ginetai eisagogi apo command line to opoio apothikeuetai stin metavliti alabama kai periexei tin akolouthia
#xaraktirwn pros anagnorisi.
alabama=raw_input ("Insert programm: ")
#ginetai diadoxiki apalifi olwn twn peritwn xaraktirwn se vimata
alabama=re.sub('[A-Z]','',alabama)
alabama=re.sub('[a-z]','',alabama)
alabama=re.sub('[0-9]','',alabama)
alabama=re.sub('[~`!@#$%^&*-_=+{};:/?.>,<]','',alabama)
alabama=re.sub("['['']']",'',alabama)
alabama=re.sub("['"'"'"]",'',alabama)
alabama=re.sub("[' ']",'',alabama)
alabama=alabama.strip()
#exoume valei ekoysia na emfanizetai h enapomeinousa akoloythia parentheseon mono
print(alabama)
#prepei na ftiaksoume kapoies stoives
#stoiva proti =>periexomeno (alfavio eisodou)
#stoiva deuteri=>katastaseis
#stoiva triti => upoloipa sumvola eisodou
sumvoloseira_parenthesewn=alabama
stack_periexomeno=["$"]
stack_katastasi=["k1"]
stack_upoloipo=[alabama]
#2 global counter gia to plithos aristerwn kai deksiwn parenthesewn
left="0"
right="0"
epanaliptiki_diadikasia=True
counter=len(alabama)
#elegxos egurotitas ws pros tin epilogi tis analutikis emfanisis ton vimatwn h mh
missisipi=True
while missisipi==True:
    epilogi=raw_input("Do you wish to print the sequence that led to result?(Y/N) ")
    if epilogi=="Y" or epilogi=="N":
        missisipi=False
    else:
        print("wrong entry try again")
#sunartisi p kanonwn
def p(parousa_katastasi,korufi_stoivas,sumvolo_eisodou):
    if parousa_katastasi=="k1" and korufi_stoivas=="$" and sumvolo_eisodou=="1":
        epomeni_katastasi="k2"
        #parousa korufi stoivas=sumvolo eisodou=1
    elif parousa_katastasi=="k1" and korufi_stoivas=="$" and sumvolo_eisodou=="2":
        epomeni_katastasi="k1"
        #parousa korufi stoivas=sumvolo eisodou=2
    elif parousa_katastasi=="k1" and korufi_stoivas=="$" and sumvolo_eisodou=="3":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=3
    elif parousa_katastasi=="k1" and korufi_stoivas=="2" and sumvolo_eisodou=="1":
        epomeni_katastasi="k2"
        #parousa korufi stoivas=sumvolo eisodou=1
    elif parousa_katastasi=="k1" and korufi_stoivas=="2" and sumvolo_eisodou=="2":
        epomeni_katastasi="k1"
        #parousa korufi stoivas=sumvolo eisodou=2
    elif parousa_katastasi=="k1" and korufi_stoivas=="2" and sumvolo_eisodou=="3":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=3
    elif parousa_katastasi=="k2" and korufi_stoivas=="1" and sumvolo_eisodou=="1":
        epomeni_katastasi="k2"
        #parousa korufi stoivas=sumvolo eisodou=1
    elif parousa_katastasi=="k2" and korufi_stoivas=="1" and sumvolo_eisodou=="2":
        epomeni_katastasi="k1"
        #parousa korufi stoivas=sumvolo eisodou=2
    elif parousa_katastasi=="k2" and korufi_stoivas=="1" and sumvolo_eisodou=="3":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=3
    elif parousa_katastasi=="k3" and korufi_stoivas=="3" and sumvolo_eisodou=="1":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=1
    elif parousa_katastasi=="k3" and korufi_stoivas=="3" and sumvolo_eisodou=="2":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=2
    elif parousa_katastasi=="k3" and korufi_stoivas=="3" and sumvolo_eisodou=="3":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=3
    #------------------------------------------
    elif parousa_katastasi=="k3" and korufi_stoivas=="2" and sumvolo_eisodou=="1":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=1
    elif parousa_katastasi=="k3" and korufi_stoivas=="2" and sumvolo_eisodou=="2":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=2
    elif parousa_katastasi=="k3" and korufi_stoivas=="2" and sumvolo_eisodou=="3":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=3
    #-----------------------------------------------------------------------
    elif parousa_katastasi=="k3" and korufi_stoivas=="1" and sumvolo_eisodou=="1":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=1
    elif parousa_katastasi=="k3" and korufi_stoivas=="1" and sumvolo_eisodou=="2":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=2
    elif parousa_katastasi=="k3" and korufi_stoivas=="1" and sumvolo_eisodou=="3":
        epomeni_katastasi="k3"
        #parousa korufi stoivas=sumvolo eisodou=3
    return epomeni_katastasi

#sunartisi genitrias sumvolou eisodou
def genitria():
    global left
    global right
    int_aristeres=int(left)
    int_deksies=int(right)
    if int_aristeres>int_deksies:
        sumvolo="1"
    elif int_aristeres==int_deksies:
        sumvolo="2"
    elif int_aristeres<int_deksies:
        sumvolo="3"
    return sumvolo

#sunartisi metrisis parenthesewn
def parentheseis_counter(parenthesi):
    global left
    global right
    int_left=int(left)
    int_right=int(right)
    if parenthesi=="(":
        int_left=int_left+1
        left=str(int_left)
    elif parenthesi==")":
        int_right=int_right+1
        right=str(int_right)

#proper sunartisi leitourgikotitas
def proper_functionality():
    global sumvoloseira_parenthesewn
    global stack_periexomeno
    global stack_katastasi
    global stack_upoloipo
    global right
    global left
    global epilogi
    if epilogi=="Y":
        print("----------------------------------------------------------------")
        print("sumvoloseira "+sumvoloseira_parenthesewn)
    prwto_stoixeio_sumvoloseiras=sumvoloseira_parenthesewn[0]
    if epilogi=="Y":
        print("prwto_stoixeio_sumvoloseiras "+prwto_stoixeio_sumvoloseiras)
    parentheseis_counter(prwto_stoixeio_sumvoloseiras)
    if epilogi=="Y":
        print("left "+left)
        print("right "+right)
    s_eisodou=genitria()
    if epilogi=="Y":
        print("s_eisodou "+s_eisodou)
    kor_stoivas_perie=stack_periexomeno[-1]
    if epilogi=="Y":
        print("kor_stoivas_perie "+kor_stoivas_perie)
    kor_stoivas_katast=stack_katastasi[-1]
    if epilogi=="Y":
        print("kor_stoivas_katast "+kor_stoivas_katast)
    ep_katastasi=p(kor_stoivas_katast,kor_stoivas_perie,s_eisodou)
    if epilogi=="Y":
        print("ep_katastasi "+ep_katastasi)
    stack_katastasi.append(ep_katastasi)
    if epilogi=="Y":
        print("korufi stoivas katastasi "+stack_katastasi[-1])
    stack_periexomeno.append(s_eisodou)
    if epilogi=="Y":
        print("periexomeno stoivas "+stack_periexomeno[-1])
    sumvoloseira_parenthesewn=sumvoloseira_parenthesewn[1:]
    if epilogi=="Y":
        print("upoloipo parenthesewn "+ sumvoloseira_parenthesewn)
        print("----------------------------------------------------------------")
while counter>0:
    proper_functionality()
    counter=counter-1

if stack_katastasi[-1]=="k1":
    print ("YES")
else:
    print("NO")
